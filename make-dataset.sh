#!/bin/sh
#set -x
config=$1

if [ ! -f datasets/$config/config ]
then
	echo "Config file 'datasets/$config/config' missing!"
	exit 1
fi

script=datasets/$config/setup.sh
if [ ! -f $script ]
then
	echo "Setup script file '$script' missing!"
	exit 1
fi

. datasets/$config/config

if [ -z "$uuid" ]
then
	echo "'uuid' can't be empty"
	exit 1
fi

if [ -z "$name" ]
then
	echo "'name' can't be empty"
	exit 1
fi

if [ -z "$version" ]
then
	echo "'version' can't be empty"
	exit 1
fi

if [ -z "$base" ]
then
	echo "'version' can't be empty"
	exit 1
fi

if ! zfs list zroot/jails/$base@final > /dev/null
then
	dependant=$(grep "uuid=$base" datasets/*/config | sed 's;datasets/\([^/]*\)/.*;\1;')
	if [ -z $dependant ]
	then
		echo "Dataset zroot/jails/$base@final does not exists."
		exit 1
	fi
	sh $0 $dependant
	if ! zfs list zroot/jails/$base@final > /dev/null
	then
		echo "Dataset zroot/jails/$base@final does not exists."
		exit 1
	fi
fi

root=zroot/jails/$uuid
if zfs list $root > /dev/null
then
	echo "Dataset $root already exists."
	exit 1
fi

echo "Generating dataset $name ($version) as $uuid"

arch=$(uname -m)
case "${ARCH}" in
    amd64)
        ARCH=x86_64;
        ;;
esac

zfs clone zroot/jails/$base@final $root

cp $script /$root/root/jail/$uuid.sh
chmod +x /$root/root/jail/$uuid.sh

chroot /$root/root/jail /$uuid.sh
rm /$root/root/jail/$uuid.sh

mkdir -p out/datasets
mkdir -p out/manifests

manifest=out/manifests/$uuid.json
dataset=out/datasets/$uuid.dataset

zfs snapshot $root@final
zfs send $root@final | bzip2 > $dataset
# zfs destroy $root

size=`ls -l $dataset | cut -f 5 -w`
sha1=`sha1 -q $dataset`
date=`date -u "+%Y-%m-%dT%H:%M:%SZ"`
cat <<EOF > $manifest
{
  "v": 2,
  "uuid": "${uuid}",
  "name": "${name}",
  "version": "${version}",
  "type": "jail-dataset",
  "os": "freebsd",
  "files": [
    {
      "size": ${size},
      "compression": "bzip2",
      "sha1": "$sha1}"
    }
  ],
  "requirements": {
    "architecture": "${arch}",
    "networks": [{"name": "net0", "description": "public"}]
  },
  "published_at": "${date}",
  "public": true,
  "state": "active",
  "disabled": false
}
EOF

img_file=/var/imgadm/images/$(echo $root | sed 's/\//-/g')-$uuid.json

echo  "{\"zpool\":\"${ROOT}\", \"manifest\":" > $img_file
cat $manifest >> $img_file
echo "}" >> $img_file
