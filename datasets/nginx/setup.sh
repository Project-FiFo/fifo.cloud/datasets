#!/bin/sh
echo 'nameserver 8.8.8.8' > /etc/resolv.conf 
ASSUME_ALWAYS_YES=yes pkg install -fy nginx
echo 'nginx_enabled="YES"' >> /etc/rc.conf
rm /etc/resolv.conf
rm -r var/cache/pkg
